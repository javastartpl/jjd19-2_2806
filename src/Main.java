import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

public class Main {
    public static void main(String[] args) {
//        BigDecimal a = BigDecimal.valueOf(0.1);
//        BigDecimal b = BigDecimal.valueOf(0.2);
        BigDecimal a = new BigDecimal("0.1");
        BigDecimal b = new BigDecimal("0.2");
        BigDecimal add = a.add(b);
        System.out.println(add);
//        System.out.println(0.1 + 0.2);
    }
}
