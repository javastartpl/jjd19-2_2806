package zad_decimal;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class PriceCalculator {

    static BigDecimal calculateNettoSum(List<Item> items) {
        BigDecimal sum = BigDecimal.ZERO;
        for (Item item : items) {
            sum = sum.add(item.getNetto());
        }
        return sum;
    }

    public static BigDecimal calculateBruttoSum(List<Item> items) {
        BigDecimal sum = BigDecimal.ZERO;
        for (Item item : items) {
            BigDecimal vat = item.getVat().divide(BigDecimal.valueOf(100));
            BigDecimal netto = item.getNetto();
            BigDecimal vatValue = netto.multiply(vat);
            BigDecimal brutto = netto.add(vatValue);
            sum = sum.add(brutto);
        }
        return sum;
    }
}
