package zad_decimal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Shop {
    public static void main(String[] args) {
        List<Item> items = new ArrayList<>();
        items.add(new Item("asdf", new BigDecimal("100"), new BigDecimal(23)));
        items.add(new Item("sdfg", new BigDecimal("234.8"), new BigDecimal(8)));
        items.add(new Item("xcvb", new BigDecimal("100"), new BigDecimal(15)));

        BigDecimal nettoSum = PriceCalculator.calculateNettoSum(items);
        System.out.println("Suma netto: " + nettoSum);
        BigDecimal bruttoSum = PriceCalculator.calculateBruttoSum(items);
        System.out.printf("Suma brutto: %.2f", bruttoSum);

    }


}
